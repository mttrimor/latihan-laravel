@extends('layouts.app')

@section('title')
Edit Data Para Pemain Film
@stop

@section('content')
<form method="POST" action="/cast">
    @csrf
    <div class="card-body">
      <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan Nama">
        @error('nama')
          <div class="alert alert-danger">
              {{ $messages }}
          </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="umur">Umur</label>
        <input type="numerik" class="form-control" id="umur" name="umur" placeholder="Masukan Umur">
        @error('nama')
          <div class="alert alert-danger">
              {{ $messages }}
          </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="bio">Biodata</label>
        <input type="text" class="form-control" id="bio" name="bio" placeholder="Bio">
        @error('nama')
          <div class="alert alert-danger">
              {{ $messages }}
          </div>
        @enderror
      </div>
            
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>

@endsection