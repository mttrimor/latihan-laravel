@extends('layouts.app')

@section('title')
Tambah Data Para Pemain Film
@stop

@section('content')
<form method="POST" action="/cast/{{$cast->id}}">
    @csrf
    @method('PUT')
    <div class="card-body">
      <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" id="nama" name="nama" value="{{$cast->nama}}">
        @error('nama')
          <div class="alert alert-danger">
              {{ $messages }}
          </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="umur">Umur</label>
        <input type="numerik" class="form-control" id="umur" name="umur" value="{{$cast->umur}}">
        @error('nama')
          <div class="alert alert-danger">
              {{ $messages }}
          </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="bio">Biodata</label>
        <input type="text" class="form-control" id="bio" name="bio" value="{{$cast->bio}}">
        @error('nama')
          <div class="alert alert-danger">
              {{ $messages }}
          </div>
        @enderror
      </div>
            
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>

@endsection