@extends('layouts.app')

@section('title')
<h4>Show Cast</h4><br>
<a href="/cast" class="btn btn-warning">Kembali</a>
@stop

@section('content')

<h2>Show Cast {{ $cast->id }}</h2>
<h4>{{ $cast->nama }}</h4>
<h4>{{ $cast->umur }}</h4>
<h4>{{ $cast->bio }}</h4>
@endsection