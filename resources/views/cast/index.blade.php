@extends('layouts.app')

@section('title')
List Data Para Pemain Film
@stop

@section('content')
<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="/cast/create"  type="button" class="btn btn-success">Tambah Data</a>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered">
          <thead>                  
            <tr>
              <th style="width: 10px">#</th>
              <th>Nama</th>
              <th>Umur</th>
              <th>Biodata</th>
              <th style="width:50px">Aksi</th>
            </tr>
          </thead>
          <tbody>
           @forelse ($casts as $key => $value)
            <tr>
              <td>{{ $key + 1 }}</td>
              <td>{{ $value->nama }}</td>
              <td>{{ $value->umur }}</td>
              <td>{{ $value->bio }}</td>
              <td>
                  <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                  <a href="/cast/{{ $value->id }}/edit" class="btn btn-primary">Edit</a>
                  <form action="/cast/{{ $value->id }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                  </form>
              </td>
            </tr>
            @empty 
            <tr colspan="5">
              <td>No Data</td>
            </tr> 
            @endforelse
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
      <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
          <li class="page-item"><a class="page-link" href="#">«</a></li>
          <li class="page-item"><a class="page-link" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item"><a class="page-link" href="#">»</a></li>
        </ul>
      </div>
    </div>
    <!-- /.card -->
    
  </div>

@endsection
