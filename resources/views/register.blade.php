
    @extends('layouts.app')
    @section('content')
    <h1 style="font-weight: bold;">Buat Account Baru!</h1>
    <h2 style="font-weight: bold;">Sign Up Form</h2>

    <form action="welcome" method="POST">
       @csrf
        <label for="">First Name :</label><br><br>
            <input type="text" name="first_name"><br><br>
        <label for="">Last Name:</label><br><br>
            <input type="text" name="last_name"><br><br>
        <label for="">Gender</label><br><br>
            <input type="radio" name="gender">Male<br>
            <input type="radio" name="gender">Female<br>
            <input type="radio" name="gender">Other<br><br>
        <label for="">Nationality</label><br><br>
        <select name="" id="">
            <option value="">Indonesia</option>
            <option value="">Malaysia</option>
            <option value="">Jerman</option>
            <option value="">Amerika</option>
            <option value="">Perancis</option>
        </select><br><br>
        <label for="">Languange Spoke</label><br><br>
            <input type="checkbox" name="Bahasa Indonesia">Bahasa Indonesia <br>
            <input type="checkbox" name="English">English <br>
            <input type="checkbox" name="other">Other <br><br>
        <label for="">Bio</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" name="submit" value="Sign Up">

    </form>

@stop